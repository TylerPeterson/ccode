## Install steps

    $ npm install -g yarn
    $ npm install -g polymer serve

## To Build

    $ yarn install

## To Run

    $ polymer serve --npm

Note: The instructions at https://www.polymer-project.org/blog/2017-08-23-hands-on-30-preview are very useful.