// my-app.js

import {Element as PolymerElement}
  from '../node_modules/@polymer/polymer/polymer-element.js';

export class MyApp extends PolymerElement {
  static get template() {
    return `<style>
      div {
        color: red;
      }
    </style>
    <link rel="import" href="./my-app.css"><div>This is my [[name]] app.</div><slot></slot>`
  }

  constructor() {
    super();
    this.name = '3.0 preview';
  }

  static get properties() {
    name: {
      Type: String
    }
  }
}

customElements.define('my-app', MyApp);